# Installation
To install simply run the standard `go install` in the gotoproject root directory.

# Setup
If you don't mind having your .bashrc file programmatically altered you can run (in the gotoproject root):
```bash
chmod +x setup.sh; . ./setup.sh
```

If you would prefer to modify your .bashrc file yourself you can simply add these two lines to the bottom of it:
```bash
function project() { cd "$(gotoproject $@)" ;}
export PROJECTS_ROOT=/path/to/your/projects/root/
```

# Usage
To use, simply type in terminal `project somestring/to/match` and gotoproject will attempt to find a folder (or path to a folder) with as many matching characters as possible and switch directories to that folder.

You can also just type pieces of folder names and gotoproject will attempt to make sense of it, for example: typing `project gosrbbtzgopr` will change directories into `$(PROJECTS_ROOT)/go/src/bitbucket.org/thezanke/gotoproject`

If no parameters are passed, it will `cd` to your `PROJECTS_ROOT` (or the default at ~).

If no matching folders are found, or the program otherwise fails, it will NOT change directories.

## Side note ##

I make an alias to "project" in my .bashrc file
```alias p=project```
that way I can just use
```p [project name]```