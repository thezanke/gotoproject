package main

import (
	"errors"
	"regexp"
	"strings"
)

// Result extends Path and adds in extra fields for the result paths
type Result struct {
	Path
}

// ResultSlice is a slice of Result structs
type ResultSlice []Result

// Results extends Paths and adds some extra functionality
type Results struct {
	items ResultSlice
}

// Append overwrites append from Path to append a Result to items
func (res *Results) Append(r Result) { res.items = append(res.items, r) }

// Count returns the legths the Results items
func (res Results) Count() int {
	return len(res.items)
}

// GetResult returns a Result from Results by index
func (res Results) GetResult(n int) Result {
	return res.items[n]
}

// GetResults returns a slice of all Result objects from Results
func (res Results) GetResults() ResultSlice {
	return res.items
}

// Search is the primary search struct
type Search struct {
	query       string
	paths       Paths
	results     Results
	pathMatcher *regexp.Regexp
}

// SetQuery sets the query property of the search object
func (search *Search) SetQuery(str string) {
	search.query = str
	search.pathMatcher = BuildPathRegexp(str)
}

// FilterPaths applies a SearchFunction to a Paths struct
func (search *Search) FilterPaths(paths Paths) error {
	var res Results

	for _, path := range paths.items {
		if search.pathMatcher.MatchString(path.ToString()) {
			res.Append(Result{path})
		}
	}

	if res.Count() == 0 {
		return errors.New("FilterPaths: No results.")
	}

	search.results = res
	return nil
}

// Search starts the search process to get the final results
func (search *Search) Search(str string) error {
	search.SetQuery(str)
	err := search.FilterPaths(search.paths)

	return err
}

// GetTopResult returns the first path in paths
func (search Search) GetTopResult() (Result, error) {
	if search.results.Count() == 0 {
		return Result{}, errors.New("GetTopResult: No results.")
	}
	return search.results.GetResult(0), nil
}

// GetAllResults returns the first path in paths
func (search Search) GetAllResults() (ResultSlice, error) {
	if search.results.Count() == 0 {
		return ResultSlice{}, errors.New("GetAllResults: No results.")
	}
	return search.results.GetResults(), nil
}

// BuildPathRegexp builds the regexp used to search the whole file path for a match
func BuildPathRegexp(s string) *regexp.Regexp {
	charSlice := strings.Split(s, "")

	for i := 0; i < len(charSlice); i++ {
		charSlice[i] = regexp.QuoteMeta(charSlice[i])
	}

	wc := ".*"
	searchStr := "(?i)^" + wc + strings.Join(charSlice, wc) + wc + "$"

	re := regexp.MustCompile(searchStr)

	return re
}

// SearchSortLess is the sort function to be used for escelation of matching results
func SearchSortLess(ps PathSlice, i, j int) bool {
	/*TODO: Need to write this sort less function*/
	return false
}

// NewSearch takes a search string
func NewSearch(str string, filtered string) (Search, error) {
	var search Search

	p, err := GetTraversedPaths(str, DefaultFilter(filtered))

	if err != nil {
		return search, err
	}

	p.Sort()
	search.paths = p

	return search, nil
}
