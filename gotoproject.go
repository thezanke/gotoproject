package main

import (
	"errors"
	"fmt"
	"log"
	"os"
	"os/user"
	"strings"
)

const (
	debug    = false
	filtered = "node_modules/bin/bower_components/assets"
)

var (
	projectsRoot string
	cwd          string
)

func getProjectsRoot() (string, error) {
	var (
		path Path
		err  error
	)

	projectsEnv := os.Getenv("PROJECTS_ROOT")

	if len(projectsEnv) != 0 {
		path, err = NewDirPath(projectsEnv)
		if err == nil {
			return path.ToString(), nil
		}
	}

	usr, err := user.Current()
	if err != nil {
		return "", err
	}

	return usr.HomeDir, nil
}

func getQuery() (string, error) {
	if len(os.Args) < 2 {
		return "", errors.New("No query passed")
	}
	return strings.Join(os.Args[1:], ""), nil
}

func printPath(str string) { fmt.Print(str); os.Exit(0) }
func printProjectsRoot()   { printPath(projectsRoot) }
func printCurrentPath()    { printPath(cwd) }

func debugLog(err error) {
	if !debug {
		return
	}

	log.Println(err)
}

func main() {
	var err error

	projectsRoot, err = getProjectsRoot()
	if err != nil {
		log.Fatal(err)
	}

	cwd, err = os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	query, err := getQuery()
	if err != nil {
		debugLog(err)
		printProjectsRoot()
	}

	search, err := NewSearch(projectsRoot, filtered)
	if err != nil {
		debugLog(err)
		printCurrentPath()
	}

	search.Search(query)

	res, err := search.GetTopResult()
	if err != nil {
		debugLog(err)
		printCurrentPath()
	}

	printPath(res.ToString())
}
