#!/bin/bash
echo "function project() { cd \"\$(gotoproject \$@)\" ;}" >> ~/.bashrc;
if [ -n "$1" ]; then
    echo "PROJECTS_ROOT=$1" >> ~/.bashrc;
fi

source ~/.bashrc