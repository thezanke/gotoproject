package main

import (
	"errors"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"sort"
	"strings"
)

// Path struct
type Path struct {
	filePath  string
	baseName  string
	dirName   string
	nestLevel int
}

// ToString returns the filepath of a Path
func (p Path) ToString() string {
	return p.filePath
}

// Base returns the Base name of a Path
func (p Path) Base() string {
	return p.baseName
}

// NestLevel returns the nestLevel of a Path
func (p Path) NestLevel() int {
	return p.nestLevel
}

// PathSlice type
type PathSlice []Path

var (
	lenFn  func(PathSlice) int
	swapFn func(PathSlice, int, int)
	lessFn func(PathSlice, int, int) bool
)

func (ps PathSlice) Len() int           { return lenFn(ps) }
func (ps PathSlice) Swap(i, j int)      { swapFn(ps, i, j) }
func (ps PathSlice) Less(i, j int) bool { return lessFn(ps, i, j) }

// Paths struct
type Paths struct {
	items  PathSlice
	filter func(string) bool
}

// SetFilter sets the Paths filter
func (p *Paths) SetFilter(filter func(string) bool) {
	p.filter = filter
}

// Append appends a new string to items
func (p *Paths) Append(path Path) { p.items = append(p.items, path) }

// SetSortLen allows you to set the Len function for sorting items
func (p *Paths) SetSortLen(fn func(PathSlice) int) { lenFn = fn }

// SetSortSwap allows you to set the Swap function for sorting items
func (p *Paths) SetSortSwap(fn func(PathSlice, int, int)) { swapFn = fn }

// SetSortLess allows you to set the Less function for sorting items
func (p *Paths) SetSortLess(fn func(PathSlice, int, int) bool) { lessFn = fn }

// Sort sorts items
func (p *Paths) Sort() {
	if lenFn == nil {
		p.SetSortLen(DefaultSortLen)
	}
	if swapFn == nil {
		p.SetSortSwap(DefaultSortSwap)
	}
	if lessFn == nil {
		p.SetSortLess(DefaultSortLess)
	}
	sort.Sort(p.items)
}

// TraversePath traverses a file path and inserts items
func (p *Paths) TraversePath(path Path) {
	matches, err := filepath.Glob(filepath.Join(path.ToString(), "/*"))

	if err != nil {
		log.Fatal(err)
	}

	for _, match := range matches {
		path, err := NewDirPath(match)

		if err != nil {
			continue
		}

		if p.filter == nil {
			p.filter = NoopFilter
		}

		if !p.filter(path.Base()) {
			p.Append(path)
			p.TraversePath(path)
		}
	}
}

// Print prints the items
func (p Paths) Print() {
	for _, item := range p.items {
		fmt.Println(item.ToString())
	}
}

// DefaultSortLen the default sort function
func DefaultSortLen(ps PathSlice) int {
	return len(ps)
}

// DefaultSortSwap the default sort function
func DefaultSortSwap(ps PathSlice, i, j int) {
	ps[i], ps[j] = ps[j], ps[i]
}

// DefaultSortLess the default sort function
func DefaultSortLess(ps PathSlice, i, j int) bool {
	pi, pj := ps[i], ps[j]
	p1, p2 := pi.ToString(), pj.ToString()
	p1s, p2s := strings.Split(p1, "/"), strings.Split(p2, "/")

	if pi.NestLevel() == pj.NestLevel() {
		return p1s[len(p1s)-1] < p2s[len(p2s)-1]
	}
	return pi.NestLevel() < pj.NestLevel()
}

// NoopFilter is a blank filter for Paths
func NoopFilter(s string) bool { return false }

// DefaultFilter creates a function to use as a filter for Paths
func DefaultFilter(filtered string) func(string) bool {
	f := strings.Split(filtered, "/")

	return func(fileName string) bool {
		if regexp.MustCompile("^\\..*$").MatchString(fileName) {
			return true
		}

		for _, filter := range f {
			if fileName == filter {
				return true
			}
		}

		return false
	}
}

// NewDirPath checks if a string is a file and if that file is a directory and creates a Path from it
func NewDirPath(fp string) (Path, error) {
	fp = filepath.Clean(fp)

	fi, err := os.Stat(fp)

	var path Path

	if err != nil {
		return path, err
	}

	if !fi.IsDir() {
		return path, errors.New("Path is not a directory")
	}

	path.filePath = fp
	path.dirName, path.baseName = filepath.Split(fp)
	path.nestLevel = len(strings.Split(fp, string(filepath.Separator))) - 1

	return path, nil
}

// GetTraversedPaths creates a new Paths struct from a file path and calls TraversePath on it
func GetTraversedPaths(fp string, filter func(string) bool) (Paths, error) {
	var paths Paths

	path, err := NewDirPath(fp)

	if err != nil {
		return paths, err
	}

	paths.SetFilter(filter)
	paths.Append(path)
	paths.TraversePath(path)

	return paths, nil
}
